def test_containers(host):
    with host.sudo():
        backend = host.docker("ce-template-backend")
        frontend = host.docker("ce-template-ui")
        assert backend.is_running
        assert frontend.is_running


def test_frontend(host):
    cmd = host.run("curl --insecure --fail https://ce-template-host/")
    assert cmd.rc == 0
    assert (
        '<meta name="description" content="Web site created using create-react-app"/>'
        in cmd.stdout
    )
