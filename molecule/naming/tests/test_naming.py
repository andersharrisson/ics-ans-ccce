def test_containers(host):
    with host.sudo():
        backend = host.docker("ce-naming-backend")
        database = host.docker("ce-naming-database")
        frontend = host.docker("ce-naming-ui")
        assert backend.is_running
        assert database.is_running
        assert frontend.is_running


def test_backend(host):
    cmd = host.run("curl --insecure --fail https://ce-naming-host/report/about")
    assert cmd.rc == 0
    assert "About Naming" in cmd.stdout


def test_frontend(host):
    cmd = host.run("curl --insecure --fail https://ce-naming-host/")
    assert cmd.rc == 0
    assert '<meta name="description" content="ce-naming-ui"/>' in cmd.stdout
